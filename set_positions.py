import pandas
import argparse
import logging
import os
import sys

logging.basicConfig(level=logging.INFO)

def main(args):
    
    filename = args.filename
    
    if not os.path.isfile(filename):
        raise IOError("File not found. Make sure the file '%s' exists and is readable." % (filename))

    outfilename = args.out
    
    if outfilename.split(".")[-1] != "py":
        raise RuntimeError("Output filename must have the extension '.py'. Aborting now, please restart.")
    
    if os.path.exists(outfilename):
        logging.warning("%s exists and will be renamed to %s.", outfilename, outfilename+".old")
        os.rename(src=outfilename, dst=outfilename+".old", src_dir_fd=None, dst_dir_fd=None)

    logging.info("Reading positions from %s .", filename)
    positions = pandas.read_csv(filename, sep=",")
    
    logging.debug("Positions = %s", repr(positions))
    
    with open("template_script.py", "r") as template:
        template_lines = template.readlines()
    
    logging.debug("Old position line reads: %s", template_lines[2])
    
    new_positions_line = '    _all_values = json.loads("""{"uploaded_csv": "source_well, destination_well'
    
    for idx in positions.index:
        new_positions_line += "\\\\n{},{}".format(positions.loc[idx, "source well"], positions.loc[idx, "destination well"])
        
    new_positions_line += '"}""")\n'
    logging.debug("New positions line = %s", new_positions_line)
    
    template_lines[2] = new_positions_line
    
    logging.info("Writing robot script to %s", outfilename)
    with open(outfilename, "w") as out:
        for line in template_lines:
            out.write(line)

    logging.info("Done.")
    
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    
    parser.add_argument("filename", type=str, metavar="FILENAME", help="Filename of csv file containing source and target positions on a well plate.")
    parser.add_argument("--outfile", "-o", type=str, dest='out', metavar="OUT", help="(Optional) filename of generated script. Defaults to 'robot.py'.", default="robot.py", required=False)
   
    args = parser.parse_args()
    
    main(args)

